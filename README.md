# Master Data

This repository contains the data for my master thesis.

## Folder Structure
* [HLoop](HLoop): Hysteresis Loop Measurements
* [SR785](SR785): Magnetic Flux Noise Measurements (with SR785)
* [MFN](MFN): Magnetic Flux Noise Measurements

## License
These data, as part of my [Lab Book compendium](https://master.ody5.de/info/license.html), is licensed under the terms of the Creative Commons Attribution 4.0 International ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)) license.

